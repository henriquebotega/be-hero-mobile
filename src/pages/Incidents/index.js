import { Feather } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import logoImg from "../../assets/logo.png";
import api from "../../services/api";
import styles from "./styles";

export default function Incidents() {
	const navigation = useNavigation();
	const [incidents, setIncidents] = useState([]);
	const [total, setTotal] = useState(0);

	const [page, setPage] = useState(1);
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		loadIncidents();
	}, [incidents]);

	const loadIncidents = async () => {
		if (loading) {
			return;
		}

		if (total > 0 && incidents.length == total) {
			return;
		}

		setLoading(true);

		const res = await api.get("/incident", { params: { page } });
		setIncidents([...incidents, ...res.data]);
		setTotal(res.headers["x-total-count"]);

		setPage(page + 1);
		setLoading(false);
	};

	const navigateToDetail = item => {
		navigation.navigate("Details", { item });
	};

	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Image source={logoImg} />
				<Text style={styles.headerText}>
					Total de <Text style={styles.headerTextBold}>{total} casos</Text>
				</Text>
			</View>

			<Text style={styles.title}>Bem vindo</Text>
			<Text style={styles.description}>Escolha um dos casos abaixo e salvo o dia.</Text>

			<FlatList
				style={styles.incidentList}
				data={incidents}
				keyExtractor={i => String(i.id)}
				showsVerticalScrollIndicator={false}
				onEndReached={loadIncidents}
				onEndReachedThreshold={0.2}
				renderItem={({ item }) => (
					<View style={styles.incident}>
						<Text style={styles.incidentProperty}>ONG:</Text>
						<Text style={styles.incidentValue}>{item.name}</Text>

						<Text style={styles.incidentProperty}>CASO:</Text>
						<Text style={styles.incidentValue}>{item.title}</Text>

						<Text style={styles.incidentProperty}>VALOR:</Text>
						<Text style={styles.incidentValue}>{Intl.NumberFormat("pt-BR", { style: "currency", currency: "BRL" }).format(item.value)}</Text>

						<TouchableOpacity style={styles.detailsButton} onPress={() => navigateToDetail(item)}>
							<Text style={styles.detailsButtonText}>Ver mais detalhes</Text>
							<Feather name="arrow-right" size={16} color="#e02041" />
						</TouchableOpacity>
					</View>
				)}
			/>
		</View>
	);
}
